from flask import Flask, request
import logzero
from logzero import logger

# Setup rotating logfile with 3 rotations, each with a maximum filesize of 1MB:
logzero.logfile("logfile.log", maxBytes=1e6, backupCount=3)

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')

@app.route('/api/<machine>/post/', methods=['POST'])
def post_request_view(machine):
    str_data = request.form.get('Wi460JSONData')
    logger.debug(str_data)
    return "Success"

if __name__ == '__main__':
    app.run(host='0.0.0.0')