from flask_script import Manager, Server
from app import app, logger

app.config.from_object('config.DevelopmentConfig')

manager = Manager(app)

manager.add_command('runserver', Server(host="0.0.0.0", port=6006))

if __name__ == '__main__':
    logger.debug("RTA Logger starting up...")
    manager.run()